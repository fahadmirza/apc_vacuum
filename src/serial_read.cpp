#include "serial/serial.h"
#include "ros/ros.h"
#include "std_msgs/Int16.h"
#include <string.h>
#include <sstream> 
#include <iostream>
#include <cstdio>
#include "apc_vacuum/ValveCtrl.h"

// OS Specific sleep
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif



using std::string;
using std::exception;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::stringstream;


 // port, baudrate, timeout in milliseconds
 serial::Serial my_serial("/dev/ttyUSB0", 9600, serial::Timeout::simpleTimeout(1000));


// Wait for miliseconds
void my_sleep(unsigned long milliseconds) 
{
#ifdef _WIN32
      Sleep(milliseconds);
#else
      usleep(milliseconds*1000);
#endif
}



bool valve_ctrl_cmd(apc_vacuum::ValveCtrl::Request  &req, apc_vacuum::ValveCtrl::Response &res)
{
	if(req.cmd == "ON")
	{
		my_serial.write("ON\n");
		//while(!my_serial.waitReadable());
		res.ack = my_serial.readline();
	}
	else if(req.cmd == "OFF")
	{
		my_serial.write("OFF\n");
		//while(!my_serial.waitReadable());
		res.ack = my_serial.readline();
	}

	//ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
	//ROS_INFO("sending back response: [%ld]", (long int)res.sum);
	return true;
}





int run(int argc, char **argv)
{
  string pressure_str;		// Contain the pressure data from arduino
  std_msgs::Int16 sensor;   // Change the response data into integer to publish in a topic
  


  ros::init(argc, argv, "SerialCom");
  ros::NodeHandle n;
  ros::Publisher serial_pub = n.advertise<std_msgs::Int16>("serial", 1000);
  ros::Rate loop_rate(20);  // Loop will run at 10hz
  
  ros::ServiceServer service = n.advertiseService("valve_control", valve_ctrl_cmd);




  cout << "Is the serial port open?";
  if(my_serial.isOpen())
    cout << " Yes." << endl;
  else
    cout << " No." << endl;



  while (ros::ok())
  {
    my_serial.write("DATA\n");
   //while(!my_serial.waitReadable());
    pressure_str = my_serial.readline();
    stringstream convert(pressure_str);
    
    if ( !(convert >> sensor.data) )//give the value to Sensor using the characters in the string
    sensor.data = 0;

	serial_pub.publish(sensor);
	
	
    ros::spinOnce();
	loop_rate.sleep();
    
  }
  cout << "\nClosing Serial Port.\n";
  my_serial.close();

  return 0;
}







int main(int argc, char **argv) 
{
  try 
  {
    return run(argc, argv);
  } 
  catch (exception &e) 
  {
    cerr << "Unhandled Exception: " << e.what() << endl;
  }
}
