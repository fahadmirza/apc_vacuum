#include "ros/ros.h"
#include "apc_vacuum/ValveCtrl.h"
#include <cstdlib>
#include <unistd.h>
#include <string.h>



using std::cout;

// Wait for miliseconds
void my_sleep(unsigned long milliseconds) 
{
	usleep(milliseconds*1000);
}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "Valve_control_client");
  

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<apc_vacuum::ValveCtrl>("valve_control");
  apc_vacuum::ValveCtrl srv;
  
  
  while (ros::ok())
  {
	  srv.request.cmd = "ON";
	  client.call(srv);
	  cout << srv.response.ack<<"\n";
	  my_sleep(2000);
	  srv.request.cmd = "OFF";
	  client.call(srv);
	  cout << srv.response.ack<<"\n";
	  my_sleep(2000);
  }
  return 0;
}

